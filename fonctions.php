<?php
	function connectMaBase(){
		// Se connecte à la BDD locale
		$base = mysql_connect ('localhost', 'root', '');
		
		if(!$base){
			die('Impossible de se connecter : ' .mysql_error());
		}
		
		// Sélectionne la BDD nommé projetweb
		$db_selected = mysql_select_db ('projetweb', $base);
		
		if (!$db_selected) {
		   die ('Impossible de sélectionner la base de données : ' . mysql_error());
		}
	}
	// Upload image pour modifier l'image de profil dans parametres.php
	function upload_avatar($avatar_tmp, $image_ext){
		if(file_exists($avatar_tmp)){
			$image_size = getimagesize($avatar_tmp);

			if($image_ext == 'jpg'){
				$image_src = imagecreatefromjpeg($avatar_tmp);
				echo "Votre image a &eacute;t&eacute; enregistr&eacute; en .jpg";
			}
			else if($image_ext == 'png'){
				$image_src = imagecreatefrompng($avatar_tmp);
				echo "Votre image a &eacute;t&eacute; enregistr&eacute; en .png";
			}
			else if($image_ext == 'gif'){
				$image_src = imagecreatefromgif($avatar_tmp);
				echo "Votre image a &eacute;t&eacute; enregistr&eacute; en .gif";
			}
			else{
				echo "...";
				$image_src = false;
			}
			//$image_src = imagecreatefromjpeg($avatar_tmp);
			imagejpeg($image_src,'pic/'.'photo_profil_user_'.$_SESSION['id_utilisateur'].'.jpg',75);
		}
	}
	
	// Upload image pour upload une image dans upload.php
	function upload_image($avatar_tmp, $image_ext){
		if(file_exists($avatar_tmp)){
			$image_size = getimagesize($avatar_tmp);

			if($image_ext == 'jpg'){
				$image_src = imagecreatefromjpeg($avatar_tmp);
				echo "Votre image a &eacute;t&eacute; enregistr&eacute; en .jpg";
			}
			else if($image_ext == 'png'){
				$image_src = imagecreatefrompng($avatar_tmp);
				echo "Votre image a &eacute;t&eacute; enregistr&eacute; en .png";
			}
			else if($image_ext == 'gif'){
				$image_src = imagecreatefromgif($avatar_tmp);
				echo "Votre image a &eacute;t&eacute; enregistr&eacute; en .gif";
			}
			else{
				echo "...";
				$image_src = false;
			}
			//$image_src = imagecreatefromjpeg($avatar_tmp);
			$today = date("y-m-d");
			$heure = date("H-i-s");
			$_SESSION['heure'] = $today.$heure;
			imagejpeg($image_src,'pic/'.'photo_'.$_SESSION['heure'].'.jpg',75);
		}
	}
?>