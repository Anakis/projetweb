
<?php
	include("fonctions.php");
	header("refresh:3;url=ajoutphoto.php");
	
	// On démarre la session AVANT d'écrire du code HTML
	session_start();
	
	// Tester si la photo de profil a été chargée
	if (isset ($_POST['charger'])){
		$avatar = $_FILES['avatar']['name'];
		$avatar_tmp = $_FILES['avatar']['tmp_name'];
		
		
		if(!empty($avatar_tmp)){
			$image = explode('.',$avatar);
			$image_ext = end($image);
			if(in_array(strtolower($image_ext),array('png','gif','jpeg','jpg')) === false)
			{
				$errors[] = "Veuillez saisir une image";
			}
		}
		
		if(empty($errors)){
			// On en registre la photo dans le dossier de la BDD (/pic)
			upload_image($avatar_tmp, $image_ext);
			// On l'ajoute à la base de données
			connectMaBase();
			$bdd = new PDO('mysql:host=localhost;dbname=projetweb;charset=utf8', 'root', '');
			
			$today = date("y-m-d");
			$iduser = $_SESSION['id_utilisateur'];
			// album
			$nomAlbum = $_POST['album'];
			// Requête pour chercher si l'album existe déjà
			$req2 = $bdd->query('SELECT a.nom, a.id_album FROM image i JOIN album a ON a.id_album = i.id_album WHERE a.id_utilisateur="'.$iduser.'" AND a.nom="'.$nomAlbum.'"') or die(mysql_error());
			$donnees = $req2->fetch();
			// Si l'album n'existe pas on le créé 
			if($donnees[0] == NULL || $donnees[0] == ""){
				$sql = 'INSERT INTO album VALUES("","'.$nomAlbum.'","'.$today.'","'.$iduser.'","1")';
				$req = mysql_query ($sql) or die ('Erreur SQL !'.$sql.'<br />'.mysql_error());
				
				$req1 = $bdd->query('SELECT id_album FROM album ORDER BY id_album DESC LIMIT 1');
				$donnee = $req1->fetch();
				$idalbum = $donnee[0]; 
				echo "album cree";
			}
			else{
				$idalbum = $donnees[1];
				echo "album existant";
			}
			
			$nomCategorie = $_POST['categorie'];
			// Requête pour chercher si la catégorie existe déjà
			$req2 = $bdd->query('SELECT c.nom, c.id_categorie FROM image i JOIN categorie c ON i.id_categorie = c.id_categorie WHERE c.nom="'.$nomCategorie.'"') or die(mysql_error());
			$donnees = $req2->fetch();
			// Si la catégorie n'existe pas on le créé 
			if($donnees[0] == NULL || $donnees[0] == ""){
				$sql = 'INSERT INTO categorie VALUES("","'.$nomCategorie.'")';
				$req = mysql_query ($sql) or die ('Erreur SQL !'.$sql.'<br />'.mysql_error());
				$req1 = $bdd->query('SELECT id_categorie FROM categorie ORDER BY id_categorie DESC LIMIT 1');
				$donnee = $req1->fetch();
				$idcategorie = $donnee[0]; 
			}
			else{
				$idcategorie = $donnees[1];
			}
			
			// visiblité
			$isPublic = $_POST['isPublic'];
			if($isPublic=="Prive" || $isPublic=="prive") $idPublic = 0;
			else if($isPublic=="Publique" || $isPublic=="publique") $idPublic = 1;
			else $idPublic = 0;
			// lieu
			$lieu = $_POST['lieu'];
			// titre
			$titre = $_POST['titre'];
			
			
			// Commande SQL d'insertion dans la base
			$today = date("y-m-d");
			$heure = date("H:i:s");
			$sql = 'INSERT INTO image VALUES("","'.$_SESSION['id_utilisateur'].'","'.$idalbum.'","'.$idcategorie.'","photo_'.$_SESSION['heure'].'.jpg","'.$titre.'","'.$lieu.'","'.$today.'","'.$heure.'","'.$idPublic.'")';
			// On lance la commande mysql query et message d'erreur si ca marche pas
			mysql_query ($sql) or die ('Erreur SQL !'.$sql.'<br />'.mysql_error());
		}
		else{
			foreach($errors as $error){
				echo $error;
			} 
		}
	}
	
	
    $dossier = 'upload/';
    $fichier = basename($_FILES['avatar']['name']);
    echo $fichier;

    
?>