<?php
	header("refresh:3;url=actualite.php");
	include("fonctions.php");
	
	// Si le bouton connecter prend une autre valeur que NULL <==> appuyé
	if (isset ($_POST['connecter'])){
		$message = '';
		// Si un des champs est vide, message erreur
		if (empty($_POST['identifiant']) || empty($_POST['password'])){
			$message = '<p>une erreur s\'est produite pendant votre identification.
		Vous devez remplir tous les champs</p>
		<p>Cliquez <a href="./accueil.html">ici</a> pour revenir</p>';
		}
		// Sinon on continue
		else{
			// On récupère les valeurs des champs de texte
			$identifiant = $_POST['identifiant'];
			$password = $_POST['password'];
			
			// On se connecte à la base
			connectMaBase();
			
			// On va chercher le password de la BDD correspondant au nom d'utilisateur
			$sql = "select id_utilisateur, mdp, prenom, nom, dateInscription, isAdmin from comptes where mail='".$identifiant."'";
			// On lance la commande mysql query et message d'erreur si ca marche pas
			$req = mysql_query ($sql) or die ('Erreur SQL !'.$sql.'<br />'.mysql_error());

			$data = mysql_fetch_assoc($req);
			
			// Si le mot de passe n'est pas bon, on ne connecte pas
			if($data['mdp'] != $password){
				$message = '<p>Une erreur s\'est produite 
			pendant votre identification.<br /> Le mot de passe ou le pseudo 
			entr&eacute; n\'est pas correcte.</p><p>Cliquez <a href="./accueil.html">ici</a>'; 
			}
			// Sinon on lance la session
			else{
				session_start();
				$_SESSION['id_utilisateur'] = $data['id_utilisateur'];
				$_SESSION['mail'] = $identifiant;
				$_SESSION['prenom'] = $data['prenom'];
				$_SESSION['nom'] = $data['nom'];
				$_SESSION['dateInscription'] = $data['dateInscription'];
				$_SESSION['mdp'] = $password;
				$_SESSION['isAdmin'] = $data['isAdmin'];
				$message = '<p>Bienvenue '.$data['prenom'].', 
				vous &ecirc;tes maintenant connect&eacute;!</p>
				<p>Cliquez <a href="./accueil.html">ici</a> 
				pour revenir &agrave; la page d\'accueil</p>'; 
			}

			// on ferme la connexion
			mysql_close();
			
			echo $message.'<p>Redirection dans 3 secondes...</p></div></body></html>';
		}
	}
?>