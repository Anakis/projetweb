<!DOCTYPE html>
<html>
    <head>
        <meta charset = "utf-8" />
        <title> Instakilogram | Paramètres </title>
        <link rel="stylesheet" type="text/css" href="parametres.css" />
		<link rel="icon" type="image/png" href="logoFavicon.png" />
    </head>

    <body>
        
        <div class="Head">
            <img src="logo.png" id="logo">
            <form id="recherche">
                <input type="text" placeholder="Amis, Albums, ..." id="barrerecherche">
            </form>
            
            <a href="ajoutphoto.php"> <img src="logo_download.png" id="droite"></a>
            <div>
                <ul id="menu_horizontal">
                    <li><a href="actualite.php">Accueil</a></li>
                    <li><a href="profil.php">Mon profil</a></li>
                    <li><a href="parametres.php">Paramètres</a></li>
                    <li><a href="deconnexion.php">Déconnexion</a></li>
					<li><a href="admin.php">Admin</a></li>
                </ul>
            </div>
        </div>
		
<?php
	// code php pour afficher la photo de profil de l'utilisateur connecté stockée en BDD
	session_start();
	$iduser = $_SESSION['id_utilisateur'];
	
	$bdd = new PDO('mysql:host=localhost;dbname=projetweb;charset=utf8', 'root', '');
				
	// Requête pour aller chercher la photo de profil de l'utilisateur connecté
	$req = $bdd->query('SELECT iprofil.photo FROM comptes c JOIN image iprofil ON iprofil.id_photo = c.id_photo WHERE c.id_utilisateur="'.$iduser.'"') or die(mysql_error());
	$donnees = $req->fetch();
	
	$repertoire = "pic/";
	$photoprofil= $donnees[0];
	$photos = "profil.png";
?>
    
        <div class="Body"> 
            <form action="parametres.php" method="post" enctype="multipart/form-data">
				
				<?php echo "<img id='photoprofil' src='".$repertoire.$photoprofil."'>"; ?>
				<input type="file" name="avatar" id="circle" style="background: url(<?php echo $photoprofil; ?>);">
				<input name="charger" value="Charger image" type="submit" id="subImage"><br/><br/>
				</input>
			</form>
            <div id="fondnoir">
                <div id="test">
                <form action="parametres.php" method="post">
					
                    <input name="newPrenom" type="text" placeholder="Nouveau Prénom" id="case"><br/><br/>
                    <input name="newNom" type="text" placeholder="Nouveau Nom"id="case"><br/><br/>
                    <input name="newMdp1" type="password" placeholder="Nouveau MDP" id="case"><br/><br/>
                    <input name="newMdp2" type="password" placeholder="Retaper MDP" id="case"><br/><br/>
                    <input name="valider" type="submit" id="sub"><br/><br/>
                </form>
                    </div>
            </div>
        </div>  
        
    </body>
</html>


<?php
	//header("refresh:3;url=parametres.html");
	include("fonctions.php");
	
	// On démarre la session AVANT d'écrire du code HTML
	session_start();
	
	if (!isset($_SESSION['mail'])) {
        // si le membre n'est pas connecté, on le redirige vers l'accueil
        header ("refresh:1;url=accueil.html");
		echo 'Vous n\'&ecirc;tes pas connect&eacute;';
        exit();
	}
	
	// Tester si la photo de profil a été chargée
	if (isset ($_POST['charger'])){
		$avatar = $_FILES['avatar']['name'];
		$avatar_tmp = $_FILES['avatar']['tmp_name'];
		
		
		if(!empty($avatar_tmp)){
			$image = explode('.',$avatar);
			$image_ext = end($image);
			if(in_array(strtolower($image_ext),array('png','gif','jpeg','jpg')) === false)
			{
				$errors[] = "Veuillez saisir une image";
			}
		}
		
		if(empty($errors)){
			// On en registre la photo dans le dossier de la BDD (/pic)
			upload_avatar($avatar_tmp, $image_ext);
			// On l'ajoute à la base de données
			connectMaBase();
			
			// Commande SQL d'insertion dans la base
			$today = date("y-m-d");
			$heure = date("H:i:s");
			$sql = 'INSERT INTO image VALUES("","'.$_SESSION['id_utilisateur'].'","1","1","photo_profil_user_'.$_SESSION['id_utilisateur'].'.jpg","Photo profil'.$_SESSION['id_utilisateur'].'","","'.$today.'","'.$heure.'","1")';
			// On lance la commande mysql query et message d'erreur si ca marche pas
			mysql_query ($sql) or die ('Erreur SQL !'.$sql.'<br />'.mysql_error());
			// on met à jour la photo de profil dans la table comptes
			$req = $bdd->query('SELECT id_photo FROM image ORDER BY id_photo DESC LIMIT 1');
			$donnees = $req->fetch();
			$lastid = $donnees[0];
			$sql = 'UPDATE comptes SET id_photo='.$lastid.' WHERE id_utilisateur='.$_SESSION['id_utilisateur'].'';
			// On lance la commande mysql query et message d'erreur si ca marche pas
			mysql_query ($sql) or die ('Erreur SQL !'.$sql.'<br />'.mysql_error());
		}
		else{
			foreach($errors as $error){
				echo $error;
			} 
		}
	}
	
	//Si le bouton valider prend une autre valeur que NULL <==> appuyé
	if (isset ($_POST['valider'])){	
		// On se connecte à la base
		connectMaBase();
		
		// Si les champs ne sont pas vides, on les sauvegarde dans une variable pour ensuite les mettre dans la bdd
		if (!empty($_POST['newPrenom'])){
			$prenom = $_POST['newPrenom'];
			echo '<p>Le prenom a bien &eacute;t&eacute; modifi&eacute;! C\'est maintenant '.$prenom.'</p>';
		}
		else $prenom = $_SESSION['prenom'];
		
		if (!empty($_POST['newNom'])){
			$nom = $_POST['newNom'];
			echo '<p>Le nom a bien &eacute;t&eacute; modifi&eacute;! C\'est maintenant '.$nom.'</p>';
		}
		else $nom = $_SESSION['nom'];
		
		if (!empty($_POST['newMdp1']) && !empty($_POST['newMdp2']) && $_POST['newMdp1']==$_POST['newMdp2'] ){
			$mdp = $_POST['newMdp1'];
			echo '<p>Le mot de passe a bien &eacute;t&eacute; modifi&eacute;! C\'est maintenant '.$mdp.'</p>';
		}
		else $mdp = $_SESSION['mdp'];
		
		// Si tous les champs sont vides
		if (empty($_POST['newPrenom']) && empty($_POST['newNom']) && (empty($_POST['newMdp1']) || empty($_POST['newMdp2']))) echo 'Aucune modification n\'a &eacute;t&eacute; effectu&eacute;e';
		
		// Création de la requête sql
		$sql = "UPDATE comptes SET nom = '$nom',
		prenom = '$prenom',
		mdp = '$mdp'
		WHERE mail = '".$_SESSION['mail']."' ";
		
		// On lance la commande mysql query et message d'erreur si ca marche pas
		mysql_query ($sql) or die ('Erreur SQL !'.$sql.'<br />'.mysql_error());
			
		// on ferme la connexion
		mysql_close();
	}
	
?>