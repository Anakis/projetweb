<!DOCTYPE html>
<html>
    <head>
        <meta charset = "utf-8" />
        <title> Instakilogram | Ajout de photo </title>
        <link rel="stylesheet" type="text/css" href="ajoutphoto.css" />
		<link rel="icon" type="image/png" href="logoFavicon.png" />
    </head>

    <body>
        
        <div class="Head">
            <img src="logo.png" id="logo">
            <form id="recherche">
                <input type="text" placeholder="Amis, Albums, ..." id="barrerecherche">
            </form>
            
            <a href="ajoutphoto.php"> <img src="logo_download.png" id="droite"></a>
            <div>
                <ul id="menu_horizontal">
                    <li><a href="actualite.php">Accueil</a></li>
                    <li><a href="profil.php">Mon profil</a></li>
                    <li><a href="parametres.php">Paramètres</a></li>
                    <li><a href="deconnexion.php">Déconnexion</a></li>
					<li><a href="admin.php">Admin</a></li>
                </ul>
            </div>
        </div>
    
        <div class="Body"> 
            <div id="fondnoir">
                <div id="add"><br/><br/><br/><br/><br/><br/><br/>
                    Cliquez pour choisir une photo ou déposez-la ici!<br/><br/><br/><br/><br/><br/>
                    <form method="POST" action="upload.php" enctype="multipart/form-data">
						 Fichier :
						<input type="file" name="avatar" id="parcourir"><br/>
						<input name="titre" type="text" style=width:500px; placeholder="Titre de la photo" id="entree"><br/>
						<input name="lieu" type="text" style=width:500px; placeholder="Lieu de la photo" id="entree"><br/>
						<input name="categorie" type="text" style=width:500px; id="entree" placeholder="Catégorie"><br/>
						<input name="isPublic" type="text" style=width:500px; id="entree" placeholder="Publique ou Prive"><br/>
						<input name="album" type="text" style=width:500px; id="entree" placeholder="Album"><br/>
						<input name="charger" type="submit" id="post" value="Poster la photo"><br/><br/>
                    </form>
                </div>
                <form>
					
                </form>
            </div>
        </div>  
        
    </body>
</html>